"""fantasy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from selection import views 
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import handler404, handler500
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', 
        login_required(views.AppDispatcher.as_view()), name="dispatcher"),
    url(r'^position/(?P<position>[-\w]+)/$', 
        login_required(views.ListOfPlayersView.as_view()), name="list-of-players"),
    url(r'^position/(?P<position>[-\w]+)/add/(?P<pk>\d+)/$',
        login_required(views.AddPlayerView.as_view()), name="add_player"),
    url(r'^position/(?P<position>[-\w]+)/remove/(?P<pk>\d+)/$',
        login_required(views.RemovePlayerView.as_view()), name="remove_player"),
    url(r'^lineup/$',
        login_required(views.LineupView.as_view()), name="lineup"),
    url(r'^week/results/$',
        login_required(views.ResultsView.as_view()), name="results"),
    url(r'^accounts/register/$',
        views.RegistrationView.as_view(), name='registration'),
    url(r'^accounts/login/$',
        views.LogInView.as_view(), name='login'),
    url(r'^accounts/logout/$',
        views.logout_view, name='logout'),
    url(r'^nav/rules/$',
        views.rules_view, name='rules'),
    url(r'^jake/$',
        login_required(views.AdminPageView.as_view()), name='admin'),
    url(r'^edit/player/(?P<pk>\d+)/$',
        csrf_exempt(login_required(views.EditPlayerView.as_view())), name='admin-edit-player'),
    url(r'^jake/release/$',
        csrf_exempt(login_required(views.ReleaseResultsButton.as_view())), name='admin-release-results'),
    url(r'^jake/new_week/$',
        csrf_exempt(login_required(views.NewWeekButton.as_view())), name='admin-new-week'),
    url(r'^foo/bar/submit/$',
        csrf_exempt(login_required(views.SubmitButton.as_view())), name='submit'),
    url(r'^foo/bar/edit/$',
        csrf_exempt(login_required(views.EditButton.as_view())), name='edit'),
]

handler404 = 'selection.views.handler404'
handler500 = 'selection.views.handler404'

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
