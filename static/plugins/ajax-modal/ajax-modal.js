/*
    Ajax Form Modal

    Ajax Form Modal accomplishes several goals:
    - be able to show a form in a modal
    - be able to render widgets when necessary
    - be able to show errors in a form in the same modal when submitted
    - reload page when submission is successful
    - all html displayed in the modal are provided via ajax by the server

    Pre-requisites:
    - Required data attributes:
        - ajax-mod-title : the title for the modal (may contain html tags)
        - ajax-mod-url : the url from which GET and POST requests will be made
        - ajax-mod-callback : a callback to execute after getting the form
    - Optional data attributes:
        - ajax-mod-lg : toggles whether to use the larger modal size; accepts boolean
*/


var template = $([
    '<div class="modal opacity-dialog-60 fade" id="ajax-modal" tabindex="-1" role="dialog" aria-hidden="true">',
    '  <div class="modal-dialog">',
    '    <div class="modal-content">',
    '      <div class="modal-header">',
    '        <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close">',
    '          <span aria-hidden="true" style="font-size:32px">×</span>',
    '        </button>',
    '        <h4 class="modal-title"></h4>',
    '      </div>',
    '      <div class="modal-body"></div>',
    '    </div>',
    '  </div>',
    '</div>',
].join('\n'));

$("body").append(template);

var ajax_modal = $("#ajax-modal");
var ajax_modal_body = ajax_modal.find('div.modal-body');

function prep_widgets(){
    // Init Datepickers
    $('input.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        clearBtn: true
    }).on(
        'changeDate',
        function(e) {
            $(this).parsley().validate();
        }
    );
}

$.fn.transformToAjax = function(){
    $(this).submit(function(e){
        var self = $(this);
        
        e.preventDefault();

        if ( self.parsley().isValid() ){
            $.ajax({
                url: self.attr('action'),
                type: "POST",
                data: new FormData(self[0]),
                processData: false,
                contentType: false,
                success: function(response){
                    if (response.success){
                        location.reload(true);
                    } else {
                        callback = self.data('callback');
                        ajax_modal_body.empty().html(response).find("form").attr('action', self.attr('action')).data('callback', callback).transformToAjax();
                        eval(callback);
                        prep_widgets();
                    }
                },
            });
        }
    });

    return this;
}

$.fn.hookAjaxModal = function(){
    $(this).click(function(){
        var self = $(this)

        ajax_modal.find('.modal-title').html(self.data('ajax-mod-title'));

        if(self.data('ajax-mod-lg') == true){
            ajax_modal.find('.modal-dialog').addClass('modal-lg');
        } else {
            if(ajax_modal.find('.modal-dialog').hasClass('modal-lg')){
                ajax_modal.find('.modal-dialog').removeClass('modal-lg');
            }
        }
        
        $.ajax({
            url: self.data('ajax-mod-url'),
            type: "GET",
            success: function(response){
                ajax_modal_body.empty().html(response).find("form").attr('action', self.data('ajax-mod-url')).data('callback', self.data('ajax-mod-callback')).transformToAjax().parsley({ requiredMessage: 'This field is required' });
                eval(self.data('ajax-mod-callback'));
                prep_widgets();
                ajax_modal.modal('show');
            }
        })
    });

    return this;
}
