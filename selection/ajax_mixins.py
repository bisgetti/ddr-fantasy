import json

from django.http import HttpResponse


class AjaxModalMixin(object):
    """
    Returns a json object when submission is successful. Also works for DeleteViews
    """
    def get_success_url(self):
        return None

    def get_success_response(self):
        return HttpResponse(json.dumps({'success': True}), content_type="application/json")

    def form_valid(self, form):
        """
        Runs the default action then returns a success json object
        """
        super(AjaxModalMixin, self).form_valid(form)
    
        return self.get_success_response()

    def delete(self, request, *args, **kwargs):
        """
        Specifically for DeleteViews, this function runs the default delete action then returns a success json object
        """
        super(AjaxModalMixin, self).delete(self, request, *args, **kwargs)

        return self.get_success_response()

class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    """
    def get_success_url(self):
        return None

    def get_success_response(self):
        return HttpResponse(json.dumps({'success': True}), content_type="application/json")

    def form_valid(self, form):
        """
        Runs the default action then returns a success json object
        """
        super(AjaxableResponseMixin, self).form_valid(form)
    
        return self.get_success_response()