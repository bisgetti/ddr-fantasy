from django import forms
from django.db import models
from django.contrib.auth.models import User
from .models import Player, Brother, Team, Password
import re
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

User = get_user_model()


class UserLoginForm(forms.Form):

    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    # every form has a clean method when doing validation
    def clean(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        # login_user = authenticate(username=username, password=password)

        user_qs = User.objects.filter(username=username)

        if user_qs.count() == 1:
            user = user_qs.first()
            if not user.check_password(password):
                error = forms.ValidationError(
                    "Incorrect password.", code="wrong_password")

                forms.Form.add_error(self, field='password', error=error)
                raise error
            elif not user.is_active:
                error = forms.ValidationError(
                    "User no longer active", code="inactive_user")
                forms.Form.add_error(self, field='username', error=error)
                raise error
        else:
            error = forms.ValidationError(
                "Username does not exist.", code="nonexistent_error")
            forms.Form.add_error(self, field='username', error=error)
            raise error

        return super(UserLoginForm, self).clean()

    def send_email(self):
        # test function
        print('email sent')

    def get_user(self):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        return authenticate(username=username, password=password)


class UserRegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'password',
            'email'
        ]
        widgets = {
            'password': forms.PasswordInput,
        }

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        if not first_name.isalpha():
            error = forms.ValidationError(
                "Firstname must be letters only.", code="bad_name")
            raise error

        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')

        if not last_name.isalpha():
            error = forms.ValidationError(
                "Lastname must be letters only.", code="bad_name")
            raise error

        return last_name

    def clean_username(self):
        username = self.cleaned_data.get('username')

        if not bool(re.match(r'^[a-zA-Z0-9]+(19|20)\d{2}([a-zA-Z]?)$', username)):
            error = forms.ValidationError(
                "Username does not follow format.", code="invalid_username")
            raise error

        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        pass_key = Password.objects.filter()[0].password
        if email != pass_key:
            error = forms.ValidationError(
                "Oops!", code="invalid_pass_key")
            raise error

        return email

    def clean_password(self):
        password = self.cleaned_data.get('password')

        if not password.isalnum():
            error = forms.ValidationError(
                "Password must be alphanumeric.", code="not_alnum")
            raise error
        elif len(password) < 8:
            error = forms.ValidationError(
                "Password must 8 characters or more.")
            raise error
        return password
