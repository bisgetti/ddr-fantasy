from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Player)
admin.site.register(Brother)
admin.site.register(Team)
admin.site.register(Week)
admin.site.register(Password)