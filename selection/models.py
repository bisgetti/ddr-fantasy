from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Password(models.Model):
    password = models.CharField(max_length=100)

    def __str__(self):
        return "Password to be used during registration"


class Week(models.Model):
    week_number = models.IntegerField()
    done = models.BooleanField(blank=True, default=False)
    results = models.BooleanField(blank=True, default=False)

    def __str__(self):
        return str.join("-", ('Week', str(self.week_number)))


class Team(models.Model):
    name = models.CharField(max_length=50)
    matchup = models.OneToOneField(
        "self", on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.name


class Player(models.Model):

    POSITIONS = (
        ('C', 'Center'),
        ('PF', 'Power Forward'),
        ('SF', 'Small Forward'),
        ('SG', 'Shooting Guard'),
        ('PG', 'Point Guard'),
    )

    fpg = models.FloatField()
    price = models.IntegerField()
    name = models.CharField(max_length=50)
    team = models.ForeignKey(
        Team, related_name="players", on_delete=models.CASCADE)
    position = models.CharField(max_length=2, choices=POSITIONS)
    photo = models.ImageField(null=True, blank=True,
                              default='media/avatar.jpg')
    default = models.BooleanField(blank=True, default=False)
    fantasy_score = models.FloatField(null=True, blank=True, default=0)

    def __str__(self):
        return str.join("-", (self.team.name, self.name))


class Brother(models.Model):
    user = models.OneToOneField(
        User, related_name="brother", on_delete=models.CASCADE)
    players = models.ManyToManyField(Player, blank=True)
    money = models.IntegerField(default=300, null=True, blank=True)
    week = models.ForeignKey(Week, related_name="challengers",
                             on_delete=models.CASCADE, null=True, blank=True)
    score = models.FloatField(null=True, blank=True, default=0)
    submitted = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Brother.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.brother.save()
