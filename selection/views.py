from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.urls import reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import DetailView, ListView, TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.edit import (CreateView, DeleteView, FormView,
                                       UpdateView)

from .ajax_mixins import AjaxModalMixin
from .forms import UserLoginForm, UserRegisterForm
from .models import Brother, Player, Team, Week
import datetime
from django.http import JsonResponse

class SubmitButton(TemplateView):
    template_name = 'selection/add_player.html'

    def post(self, request, *args, **kwargs):
        brother = Brother.objects.get(user=self.request.user)
        brother.submitted = True
        brother.save()
        return redirect(reverse('lineup'))


class EditButton(TemplateView):
    template_name = 'selection/add_player.html'

    def post(self, request, *args, **kwargs):
        brother = Brother.objects.get(user=self.request.user)
        brother.submitted = False
        position = self.request.POST['position']
        brother.save()
        return redirect(reverse('list-of-players', kwargs={'position':position}))

class ReleaseResultsButton(TemplateView):
    template_name = 'selection/add_player.html'

    def post(self, request, *args, **kwargs):
        latest_week = Week.objects.all().latest('id')
        latest_week.results = True
        latest_week.save()
        return JsonResponse({'process': 'ok'})

class NewWeekButton(TemplateView):
    template_name = 'selection/add_player.html'

    def post(self, request, *args, **kwargs):
        latest_week = Week.objects.all().latest('id')
        latest_week.done = True
        new_week = Week.objects.create(week_number=latest_week.id+1)
        Player.objects.filter().update(fantasy_score=0)
        brothers = Brother.objects.all()
        for brother in brothers:
            brother.score = 0
            brother.submitted = False
            brother.week = new_week
            previous_players = brother.players.all()
            for player in previous_players:
                brother.players.remove(player)
            brother.save()

        return JsonResponse({'process': 'ok'})
    
class AdminPageView(ListView, LoginRequiredMixin):
    template_name = "selection/admin.html"
    model = Player
    context_object_name = "players"

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            return redirect('/')
        return super(AdminPageView, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(AdminPageView, self).get_context_data(*args, **kwargs)
        if datetime.time(12) > datetime.datetime.now().time():
            context['greetings'] = "evening"
        else:
            context['greetings'] = "morning"
        return context


class EditPlayerView(TemplateView):

    template_name = 'selection/add_player.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            return redirect('/')
        return super(EditPlayerView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            field = self.request.POST['field']
            value = self.request.POST['value']
            player = Player.objects.get(id=self.kwargs['pk'])
            if field == 'fpg':
                player.fpg = float(value)
            elif field == 'fantasy_score':
                player.fantasy_score = float(value)
            elif field == 'price':
                player.price = int(value)
            player.save()
        except ValueError:
            return JsonResponse({'process': 'in progress'})
        return JsonResponse({'process': 'ok'})

class AppDispatcher(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        brother = Brother.objects.get(user=self.request.user)
        week = Week.objects.all().latest('id')
        if week.results:
            url = reverse('lineup')
        else:
            url = reverse('list-of-players', kwargs={'position': 'C'})
        return url


class ListOfPlayersView(
    ListView,
    LoginRequiredMixin,
):
    '''
        Lists the available players
    '''

    template_name = "selection/read_players.html"
    model = Player
    context_object_name = "players"
    position = 'C'

    def dispatch(self, request, *args, **kwargs):
        self.position = self.kwargs['position']
        brother = Brother.objects.get(user=self.request.user)
        week = Week.objects.all().latest('id')

        if not week.results and not week.done:
            return super(ListOfPlayersView, self).dispatch(request,
                                                           *args, **kwargs)

        elif brother.submitted or (week.results and not week.done):
            url = reverse('lineup')
            return redirect(url)
        

        

    def get_context_data(self, *args, **kwargs):
        context = super(ListOfPlayersView,
                        self).get_context_data(*args, **kwargs)
        latest_week = Week.objects.all().latest('id')
        if latest_week.done and latest_week.results:
            brother = Brother.objects.get(user=self.request.user)
            previous_players = brother.players.all()
            for player in previous_players:
                brother.players.remove(player)

        positions = ['C', 'PF', 'SF', 'SG', 'PG']
        position = self.kwargs['position'].upper()

        if position not in positions:
            position = 'C'

        list_of_players = Player.objects.filter(position=position,
                                                default=False).order_by('-price', '-fpg')

        chosen_players = Brother.objects.get(
            user=self.request.user).players.all()

        pg = Player.objects.get(default=True)
        sg = Player.objects.get(default=True)
        sf = Player.objects.get(default=True)
        pf = Player.objects.get(default=True)
        c = Player.objects.get(default=True)

        context['c'] = c
        context['pf'] = pf
        context['sf'] = sf
        context['sg'] = sg
        context['pg'] = pg

        budget = 300

        for player_position in positions:
            for player in chosen_players:
                if player.position == player_position:
                    context[player_position.lower()] = player

        for player in chosen_players:
            budget -= player.price

        if budget >= 0 and chosen_players.count() == 5:
            context['is_submittable'] = True
        else:
            context['is_submittable'] = False

        context['user_id'] = self.request.user.id

        try:
            context['current_price'] = chosen_players.get(
                position=position).price + budget
        except:
            context['current_price'] = budget

        context['budget'] = budget

        size_c, size_pf, size_sf, size_pg, size_sg = 100, 100, 100, 100, 100

        if position == 'C':
            size_c = 120
        elif position == 'PF':
            size_pf = 120
        elif position == 'SF':
            size_sf = 120
        elif position == 'PG':
            size_pg = 120
        else:
            size_sg = 120

        context['size_c'] = size_c
        context['size_pf'] = size_pf
        context['size_sf'] = size_sf
        context['size_sg'] = size_sg
        context['size_pg'] = size_pg

        context['chosen_players'] = chosen_players
        context['list_of_players'] = list_of_players

        top_row = []
        bottom_row = []
        need_extra = False

        for i in range(0, len(list_of_players), 2):
            try:
                top_row.append(list_of_players[i])
                bottom_row.append(list_of_players[i+1])
            except:
                need_extra = True

        context['top_row'] = top_row
        context['bottom_row'] = bottom_row
        context['need_extra'] = need_extra

        return context


class AddPlayerView(
    LoginRequiredMixin,
    UpdateView,
):
    template_name = 'selection/add_player.html'

    model = Player
    fields = [
        'name'
    ]
    context_object_name = "obj"

    def get_context_data(self, *args, **kwargs):
        context = super(AddPlayerView,
                        self).get_context_data(*args, **kwargs)
        user = Brother.objects.get(user=self.request.user)
        new_player = Player.objects.get(pk=self.kwargs['pk'])
        try:
            previous_player = user.players.all().get(
                position=self.kwargs['position'], default=False)
            user.players.remove(previous_player)
        except:
            pass
        user.players.add(new_player)

        return context


class RemovePlayerView(
    LoginRequiredMixin,
    UpdateView,
):
    template_name = 'selection/add_player.html'

    model = Player
    fields = [
        'name'
    ]
    context_object_name = "obj"

    def get_context_data(self, *args, **kwargs):
        context = super(RemovePlayerView,
                        self).get_context_data(*args, **kwargs)
        user = Brother.objects.get(user=self.request.user)
        previous_player = user.players.all().get(
            pk=self.kwargs['pk'])
        user.players.remove(previous_player)
        return context


class LineupView(
    TemplateView,
    LoginRequiredMixin,
):
    model = Brother
    context_object_name = "brother"
    template_name = "selection/lineup.html"

    def dispatch(self, *args, **kwargs):
        brother = Brother.objects.get(user=self.request.user)
        latest_week = Week.objects.all().latest('id')
        if latest_week.results and not brother.submitted:
            url = reverse('results')
            return redirect(url)
        elif not brother.submitted:
            url = reverse('list-of-players', kwargs={'position':'C'})
            return redirect(url)
        return super(LineupView, self).dispatch(*args, **kwargs)

    # add handler
    def get_context_data(self, *args, **kwargs):
        context = super(LineupView,
                        self).get_context_data(*args, **kwargs)
        user = Brother.objects.get(
            user=self.request.user)
        user.submitted = True
        user.save()

        chosen_players = user.players.all()

        current_week = Week.objects.all().latest('id')
        user.week = current_week
        user.save()

        c = chosen_players.get(position='C')
        pf = chosen_players.get(position='PF')
        sf = chosen_players.get(position='SF')
        sg = chosen_players.get(position='SG')
        pg = chosen_players.get(position='PG')

        context['are_results_done'] = current_week.results
        context['c'] = c
        context['pf'] = pf
        context['sf'] = sf
        context['sg'] = sg
        context['pg'] = pg

        return context


class ResultsView(
    LoginRequiredMixin,
    ListView
):

    model = Brother
    context_object_name = "brothers"
    template_name = "selection/results.html"

    def dispatch(self, request, *args, **kwargs):
        latest_week = Week.objects.all().latest('id')
        if latest_week.results and not latest_week.done:
            return super(ResultsView, self).dispatch(request, *args, **kwargs)
        return redirect(reverse('list-of-players', kwargs={'position': 'C'}))
        

    def get_context_data(self, *args, **kwargs):
        context = super(ResultsView,
                        self).get_context_data(*args, **kwargs)
        current_week = Week.objects.all().latest('id')
        participants = current_week.challengers.all().filter(submitted=True)

        for participant in participants:
            score = 0
            for chosen_player in participant.players.all():
                score += chosen_player.fantasy_score
            participant.score = score
            participant.save()
        context['results'] = participants.order_by('-score')
        context['max_score'] = participants.order_by('-score')[0].score
        return context


def rules_view(request):
    return render(request, 'selection/rules.html', context={})


def logout_view(request):
    logout(request)

    return redirect("/")

class RegistrationView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = UserRegisterForm
    success_url = '/'

    def form_valid(self, form, *args, **kwargs):
        user = form.save(commit=False)
        user.set_password(user.password)
        user.save()
        login(self.request, user)
        return redirect(self.success_url)

class LogInView(FormView):
    template_name = 'login.html'
    form_class = UserLoginForm
    success_url = '/'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super().form_valid(form)


def handler404(request):
    return render(request, 'selection/error404.html', context={})
